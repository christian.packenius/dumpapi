package com.packenius.dumpapi;

/**
 * A "cross pointer" leads to another dump block anywhere within the full
 * content dump. This not necessary has to be a child or parent block. The
 * destination dump has to be asked for at the main dump block.
 * @author Christian Packenius, 2019.
 */
public interface CrossPointer {
    // No content - realized by implementation.
}
