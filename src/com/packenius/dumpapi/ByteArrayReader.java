package com.packenius.dumpapi;

/**
 * Reading a byte array byte for byte.
 * @author Christian Packenius, 2017.
 */
class ByteArrayReader {
    /**
     * Byte array content.
     */
    private final byte[] content;

    /**
     * Byte array length.
     */
    public final int length;

    /**
     * Index into upper byte array.
     */
    private int index;

    /**
     * Constructor, will NOT deep copy byte array content but hold reference on it!
     * @param content Byte array with the file content to be partitionally read.
     */
    public ByteArrayReader(byte[] content) {
        this.content = content;
        length = content.length;
    }

    /**
     * Reads an unsigned 64 bit big endian value from byte array content.
     * @return Read big endian value.
     */
    protected long readBigEndianU8() {
        checkWantedBytes(8);
        long value = content[index++] & 255;
        value <<= 8;
        value |= content[index++] & 255;
        value <<= 8;
        value |= content[index++] & 255;
        value <<= 8;
        value |= content[index++] & 255;
        value <<= 8;
        value |= content[index++] & 255;
        value <<= 8;
        value |= content[index++] & 255;
        value <<= 8;
        value |= content[index++] & 255;
        value <<= 8;
        value |= content[index++] & 255;
        return value;
    }

    /**
     * Reads an unsigned 32 bit big endian value from byte array content.
     * @return Read big endian value.
     */
    protected long readBigEndianU4() {
        checkWantedBytes(4);
        long value = content[index++] & 255;
        value <<= 8;
        value |= content[index++] & 255;
        value <<= 8;
        value |= content[index++] & 255;
        value <<= 8;
        value |= content[index++] & 255;
        return value;
    }

    /**
     * Reads an signed 32 bit big endian value from byte array content.
     * @return Read big endian value.
     */
    protected int readBigEndianS4() {
        checkWantedBytes(4);
        int value = content[index++] & 255;
        value <<= 8;
        value |= content[index++] & 255;
        value <<= 8;
        value |= content[index++] & 255;
        value <<= 8;
        value |= content[index++] & 255;
        return value;
    }

    /**
     * Reads an unsigned 16 bit big endian value from byte array content.
     * @return Read big endian value.
     */
    protected int readBigEndianU2() {
        checkWantedBytes(2);
        int value = content[index++] & 255;
        value <<= 8;
        value |= content[index++] & 255;
        return value;
    }

    /**
     * @return Read big endian value. Reads an signed 162 bit big endian value from
     * byte array content.
     */
    protected short readBigEndianS2() {
        checkWantedBytes(2);
        short value = content[index++];
        value <<= 8;
        value |= content[index++] & 255;
        return value;
    }

    /**
     * Reads an unsigned 8 bit value from byte array content.
     * @return Read byte value.
     */
    protected short readU1() {
        checkWantedBytes(1);
        return (short) (content[index++] & 255);
    }

    /**
     * Reads a signed 8 bit value from byte array content.
     * @return Read byte value.
     */
    protected byte readS1() {
        checkWantedBytes(1);
        return content[index++];
    }

    /**
     * Reads a sub byte array of the given length into a new created byte array.
     */
    public byte[] readBytes(int length) {
        checkWantedBytes(length);
        byte[] ba = new byte[length];
        System.arraycopy(content, index, ba, 0, length);
        index += length;
        return ba;
    }

    private void checkWantedBytes(int wantedBytes) {
        if (index + wantedBytes > length) {
            throw new ArrayIndexOutOfBoundsException(
                    "Zu wenige Bytes vorhanden! index=" + index + ", length=" + length);
        }
    }

    /**
     * Returns the current index within the byte array.
     */
    public int getCurrentIndex() {
        return index;
    }

    /**
     * Checks of end of byte array is reached.
     */
    public boolean isEOF() {
        return index >= length;
    }

    /**
     * Returns full (original!) byte array content reference.
     */
    public byte[] getContent() {
        return content;
    }

    /**
     * Changes byte array index relative.
     */
    public void deltaIndex(int delta) {
        index += delta;
    }

    /**
     * Returns current byte without changing current byte array index.
     */
    public int getCurrentByte() {
        return content[index];
    }

    /**
     * Returns the byte with given index delta.
     */
    public int getDeltaByte(int delta) {
        return content[index + delta];
    }
}
