package com.packenius.dumpapi;

public class Statics {
    /**
     * Creates a string containing only '0' and '1' characters that represents the
     * lower <count> bits of the given long value.
     * @param value Long value to be converted into binary string.
     * @param count Number of bits to converted. These will be the lower bits of the
     * long value.
     */
    public static String toBinaryString(long value, int count) {
        char[] ca = new char[count];
        long bit = 1;
        while (count-- > 0) {
            if ((value & bit) != 0) {
                ca[count] = '1';
            } else {
                ca[count] = '0';
            }
            bit <<= 1;
        }
        return new String(ca);
    }

    /**
     * Count the real numbers of unicode characters within the given string.<br>
     * The algorithm is quite easy: Ignore just ignore one part of the surrogates.
     */
    public static int countUtf8Characters(String string) {
        int count = 0;
        for (char ch : string.toCharArray()) {
            if (ch < '\uD800' || ch > '\uDBFF') {
                count++;
            }
        }
        return count;
    }
}
