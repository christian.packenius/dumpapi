package com.packenius.dumpapi;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Stack;

public class DumpReader extends ByteArrayReader {
    /**
     * Collects all child dump blocks of a given parent dump block.
     */
    private final Map<DumpBlock, List<DumpBlock>> blocksMap = new HashMap<>();

    /**
     * Every dump block that has been started but not yet finished being dumped.
     */
    private final Stack<DumpBlock> currentHierarchyBlocks = new Stack<>();

    /**
     * Main dump block of the dumped content.
     */
    private MainDumpBlock mainBlock;

    public DumpReader(byte[] content) {
        super(content);
    }

    /**
     * Start reading of a dump block.
     */
    void startDumpBlock(DumpBlock dumpBlock) {
        // Is this the main dump block?
        if (currentHierarchyBlocks.isEmpty()) {
            if (!blocksMap.isEmpty()) {
                throw new RuntimeException("More than one MainDumpBlock not allowed!");
            }
            if (!(dumpBlock instanceof MainDumpBlock)) {
                throw new RuntimeException("Top dump block has to be of type MainDumpBlock!");
            }
            mainBlock = (MainDumpBlock) dumpBlock;
        }

        // No, just a child dump block of any hierarchy level.
        else {
            List<DumpBlock> list = blocksMap.get(currentHierarchyBlocks.peek());
            if (list == null) {
                list = new ArrayList<>();
                blocksMap.put(currentHierarchyBlocks.peek(), list);
            }

            // Store block.
            list.add(dumpBlock);
        }

        // Store in hierarchy.
        currentHierarchyBlocks.push(dumpBlock);
    }

    /**
     * Finishes the reading of a dump block.
     */
    void endDumpBlock(DumpBlock dumpBlock) {
        if (!currentHierarchyBlocks.isEmpty()) {
            DumpBlock chb = currentHierarchyBlocks.pop();
            if (dumpBlock != chb) {
                throw new RuntimeException("Wrong dump block ended: " + chb.getClass().getSimpleName() + " vs "
                        + dumpBlock.getClass().getSimpleName());
            }
        }
    }

    public MainDumpBlock getMainBlock() {
        checkAll();
        if (mainBlock == null) {
            throw new RuntimeException("Missing main dump block!");
        }
        return mainBlock;
    }

    private boolean checked;

    /**
     * Checks all blocks of being consistent.
     */
    private void checkAll() {
        if (!checked) {
            check(Arrays.asList(mainBlock), 0);
            checked = true;
        }
    }

    /**
     * Checks adresses of the blocks recursive.
     */
    private int check(List<DumpBlock> blocks, int address) {
        if (blocks == null) {
            return address;
        }

        for (int i = 0; i < blocks.size(); i++) {
            DumpBlock block = blocks.get(i);

            if (address > block.getStartAddress()) {
                throw new RuntimeException("Overlapping blocks!");
            }

            List<DumpBlock> children = blocksMap.get(block);
            int endAddress = check(children, address);
            if (endAddress < address) {
                throw new RuntimeException("Block with startAddress > endAddress!");
            }
            address = endAddress;
        }

        return address;
    }

    /**
     * Storage for user objects.
     */
    private final Map<Class<?>, Object> userObjects = new HashMap<Class<?>, Object>();

    public void setUserObject(Object object) {
        if (userObjects.containsKey(object.getClass())) {
            throw new RuntimeException("Object of class " + object.getClass() + " already exists!");
        }
        userObjects.put(object.getClass(), object);
    }

    @SuppressWarnings("unchecked")
    public <T> T getUserObject(Class<T> clazz) {
        return (T) userObjects.get(clazz);
    }

    public List<DumpBlock> getSubBlocks(DumpBlock block) {
        return blocksMap.get(block);
    }

    public List<DumpBlock> getSubBlocks(DumpBlock block, Class<? extends DumpBlock> clazz) {
        List<DumpBlock> result = getSubBlocks(block);
        for (int i = result.size() - 1; i >= 0; i--) {
            if (!clazz.isInstance(result.get(i))) {
                result.remove(i);
            }
        }
        return result;
    }

    private String changeTitle(String title, long value) {
        title = title == null ? "" : title;
        title = title.replace("##DEC##", Long.toString(value));
        title = title.replace("##BIN1##", Statics.toBinaryString(value, 8));
        title = title.replace("##BIN2##", Statics.toBinaryString(value, 16));
        title = title.replace("##BIN4##", Statics.toBinaryString(value, 32));
        title = title.replace("##BIN8##", Statics.toBinaryString(value, 64));
        title = title.replace("##HEX1##", hexString(value, 2));
        title = title.replace("##HEX2##", hexString(value, 4));
        title = title.replace("##HEX4##", hexString(value, 8));
        title = title.replace("##HEX8##", hexString(value, 16));
        title = title.replace("##FLOAT##", Float.toString(Float.intBitsToFloat((int) value)));
        title = title.replace("##DOUBLE##", Double.toString(Double.longBitsToDouble(value)));
        title = title.replace("##CHAR##", "" + ((char) (byte) value));
        if (title.contains("##")) {
            throw new RuntimeException("title with ##: " + title);
        }
        return title;
    }

    private static char[] HEX = "0123456789abcdef".toCharArray();

    public static String hexString(long value, int count) {
        char[] ca = new char[count];
        long bits = 15;
        int shift = 0;
        while (count-- > 0) {
            ca[count] = HEX[15 & (int) ((value & bits) >> shift)];
            shift += 4;
            bits <<= 4;
        }
        return new String(ca);
    }

    // Methods for reading small values. Own DumpBlock class for every type.

    public Unsigned1ByteInteger readU1(String title) {
        return new Unsigned1ByteInteger(title); // .value;
    }

    public class Unsigned1ByteInteger extends DumpBlock {
        public final short value;

        private final String title;

        public Unsigned1ByteInteger(String title) {
            super(DumpReader.this);
            this.value = DumpReader.super.readU1();
            this.title = changeTitle(title, value);
            setEndAddress(DumpReader.this);
        }

        @Override
        public String toString() {
            return title;
        }

        @Override
        public String getDescription() {
            return "Single unsigned byte with decimal value " + value + ".";
        }
    }

    public BigEndianUnsigned2ByteInteger readBigEndianU2(String title) {
        return new BigEndianUnsigned2ByteInteger(title);
    }

    public class BigEndianUnsigned2ByteInteger extends DumpBlock {
        public final int value;

        private final String title;

        public BigEndianUnsigned2ByteInteger(String title) {
            super(DumpReader.this);
            this.value = DumpReader.super.readBigEndianU2();
            this.title = changeTitle(title, value);
            setEndAddress(DumpReader.this);
        }

        @Override
        public String toString() {
            return title;
        }

        @Override
        public String getDescription() {
            return "Two unsigned bytes (in big endian format) with decimal value " + value + ".";
        }
    }

    public long readBigEndianU4(String title) {
        return new BigEndianUnsigned4ByteInteger(title).value;
    }

    public class BigEndianUnsigned4ByteInteger extends DumpBlock {
        private final long value;
        private final String title;

        public BigEndianUnsigned4ByteInteger(String title) {
            super(DumpReader.this);
            this.value = DumpReader.super.readBigEndianU4();
            this.title = changeTitle(title, value);
            setEndAddress(DumpReader.this);
        }

        @Override
        public String toString() {
            return title;
        }

        @Override
        public String getDescription() {
            return "Four unsigned bytes (in big endian format) with decimal value " + value + ".";
        }
    }

    public long readBigEndianU8(String title) {
        return new BigEndianUnsigned8ByteInteger(title).value;
    }

    public class BigEndianUnsigned8ByteInteger extends DumpBlock {
        private final long value;
        private final String title;

        public BigEndianUnsigned8ByteInteger(String title) {
            super(DumpReader.this);
            this.value = DumpReader.super.readBigEndianU8();
            this.title = changeTitle(title, value);
            setEndAddress(DumpReader.this);
        }

        @Override
        public String toString() {
            return title;
        }

        @Override
        public String getDescription() {
            return "Eight signed bytes (in big endian format) with decimal value " + value + ".";
        }
    }

    public int readBigEndianS4(String title) {
        return new BigEndianSigned4ByteInteger(title).value;
    }

    public class BigEndianSigned4ByteInteger extends DumpBlock {
        private final int value;
        private final String title;

        public BigEndianSigned4ByteInteger(String title) {
            super(DumpReader.this);
            this.value = DumpReader.super.readBigEndianS4();
            this.title = changeTitle(title, value);
            setEndAddress(DumpReader.this);
        }

        @Override
        public String toString() {
            return title;
        }

        @Override
        public String getDescription() {
            return "Four signed bytes (in big endian format) with decimal value " + value + ".";
        }
    }

    public short readBigEndianS2(String title) {
        return new BigEndianSigned2ByteInteger(title).value;
    }

    public class BigEndianSigned2ByteInteger extends DumpBlock {
        private final short value;
        private final String title;

        public BigEndianSigned2ByteInteger(String title) {
            super(DumpReader.this);
            this.value = DumpReader.super.readBigEndianS2();
            this.title = changeTitle(title, value);
            setEndAddress(DumpReader.this);
        }

        @Override
        public String toString() {
            return title;
        }

        @Override
        public String getDescription() {
            return "Two signed bytes (in big endian format) with decimal value " + value + ".";
        }
    }

    public byte readS1(String title) {
        return new Signed1ByteInteger(title).value;
    }

    public class Signed1ByteInteger extends DumpBlock {
        private final byte value;
        private final String title;

        public Signed1ByteInteger(String title) {
            super(DumpReader.this);
            this.value = DumpReader.super.readS1();
            this.title = changeTitle(title, value);
            setEndAddress(DumpReader.this);
        }

        @Override
        public String toString() {
            return title;
        }

        @Override
        public String getDescription() {
            return "Single signed byte with decimal value " + value + ".";
        }
    }

    public byte[] readBytes(int length, String title) {
        return new Bytes(length, title).value;
    }

    public class Bytes extends DumpBlock {
        private final byte[] value;
        private final String title;
        private final boolean withUTF8;

        public Bytes(int length, String title) {
            super(DumpReader.this);
            this.value = DumpReader.super.readBytes(length);
            if (title == null) {
                withUTF8 = false;
                this.title = null;
            } else {
                withUTF8 = title.contains("##UTF8##");
                this.title = title.replace("##UTF8##", new String(value, StandardCharsets.UTF_8));
            }
            setEndAddress(DumpReader.this);
        }

        @Override
        public String toString() {
            return title;
        }

        @Override
        public String getDescription() {
            if (withUTF8) {
                String string = new String(value, StandardCharsets.UTF_8);
                return "Range of " + value.length + " bytes, resulting in an string containing "
                        + Statics.countUtf8Characters(string) + " characters.";
            }
            return "Range of " + value.length + " bytes.";
        }
    }

    @Override
    public void deltaIndex(int delta) {
        super.deltaIndex(delta);
        List<DumpBlock> children = blocksMap.get(currentHierarchyBlocks.peek());
        while (delta < 0) {
            int byteSize = children.get(children.size() - 1).getByteSize();
            if (byteSize <= -delta) {
                delta += byteSize;
                children.remove(children.size() - 1);
            } else {
                break;
            }
        }
    }
}
