package com.packenius.dumpapi;

/**
 * Top dump block of a dump block tree.
 * @author Christian Packenius, 2019.
 */
public abstract class MainDumpBlock extends DumpBlock {
    public MainDumpBlock(DumpReader reader) {
        super(reader);
    }

    /**
     * Get the real dump block that a cross pointer points to.
     */
    public abstract DumpBlock getCrossPointerDumpBlocks(CrossPointer crossPointer);
}
