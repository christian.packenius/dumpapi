package com.packenius.dumpapi;

/**
 * Pure marker interface telling that this block not has to be splitted down
 * within a hex dump.
 * @author Christian Packenius, 2019.
 */
public interface TechnicalDumpBlock {
}
